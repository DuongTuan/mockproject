//
//  EventsCurrentAndUpComing.swift
//  MockProject
//
//  Created by AnhDCT on 10/9/19.
//  Copyright © 2019 AnhDCT. All rights reserved.
//

import UIKit

class EventsCurrentAndUpComing: UITableViewController {
    var currenEvents = [EventsStruct]()
    private let spinner = UIActivityIndicatorView(style: .gray)
    var key = ""
    private var pageIndex = 1
    private var pageSize = 20
    private var isLoading = false

    override func viewDidLoad() {
        super.viewDidLoad()
        registerForCell()
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        NotificationCenter.default.addObserver(self, selector: #selector(getAPI), name: NSNotification.Name("key"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(removeData), name: NSNotification.Name(rawValue: "deleteData"), object: nil)
    }
    
    private func getData() {
        let url = "\(baseURL)search?token=\(User.instance.token ?? "" )&keyword=\(key)&pageIndex=\(pageIndex)&pageSize=\(pageSize)"
        getGenericData(urlString: url) { (json: PopularStruct) in
            DispatchQueue.main.async {
                json.response.events.forEach({ (event) in
                    guard let end = event.schedule_end_date else {return}
                    let currentDate  = Date()
                    let dateFormat = "yyyy-MM-dd"
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = dateFormat
                    guard let endDate = dateFormatter.date(from: end) else {return}
                    if endDate > currentDate {
                        self.currenEvents.append(event)
                        self.currenEvents.sort(by: { (a, b) -> Bool in
                            a.going_count ?? 0 > b.going_count ?? 0
                        })
                    }
                })
                self.tableView.reloadData()
                self.spinner.stopAnimating()
                self.tableView.tableFooterView?.isHidden = true
                self.isLoading = false
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "numberCurrentAndUpComing"), object: self, userInfo: ["numberC" : self.currenEvents.count])
            }
        }
    }
    
    @objc func getAPI(notification : Notification) {
        key = notification.userInfo?["text"] as! String
        getData()
        tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
    }
    
    @objc func removeData() {
        currenEvents = []
        pageIndex = 1
    }
    
    @objc func refreshData() {
        currenEvents = []
        pageIndex = 1
        getData()
        refreshControl?.endRefreshing()
    }

    private func registerForCell() {
        tableView.register(UINib(nibName: "PopularTableViewCell", bundle: nil), forCellReuseIdentifier: "PopularTableViewCell")
    }
    
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        if !isLoading && offsetY > contentHeight - scrollView.frame.size.height - 10 {
            pageIndex += 1
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            tableView.tableFooterView = spinner
            tableView.tableFooterView?.isHidden = false
            isLoading = true
            getData()
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if currenEvents.isEmpty{
            return 1
        } else {
            return currenEvents.count
        }
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if currenEvents.isEmpty {
            let cell = UITableViewCell()
            cell.textLabel?.textAlignment = .center
            cell.textLabel?.textColor = .black
            cell.textLabel?.font = UIFont(name: "Lato-Bold", size: 18 )
            cell.textLabel?.text = "“Không có sự kiện phù hợp”"
            cell.selectionStyle = .none
            tableView.isScrollEnabled = false
            tableView.separatorStyle = .none
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PopularTableViewCell", for: indexPath) as! PopularTableViewCell
            cell.setupData(events: currenEvents[indexPath.row])
            tableView.isScrollEnabled = true
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if currenEvents.isEmpty{
            return
        } else {
            let eventsDetailVC = EventsDetailViewController(id: currenEvents[indexPath.row].id ?? 1)
            present(eventsDetailVC, animated: true, completion: nil)
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if currenEvents.isEmpty{
            return tableView.frame.height
        }
        tableView.estimatedRowHeight = 300
        return UITableView.automaticDimension
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
