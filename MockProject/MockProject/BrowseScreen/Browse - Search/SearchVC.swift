//
//  SearchVC.swift
//  MockProject
//
//  Created by AnhDCT on 9/23/19.
//  Copyright © 2019 AnhDCT. All rights reserved.
//

import UIKit

class SearchVC: UIViewController {
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var segEvents: UISegmentedControl!
    @IBOutlet weak var scrollTab: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollTab.isScrollEnabled = false
        scrollTab.contentSize.width = view.frame.width * 2
        setupScrollTab()
        tabBarController?.tabBar.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(seg2), name: NSNotification.Name("numberPast"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(seg1), name: NSNotification.Name("numberCurrentAndUpComing"), object: nil)
    }
    
    @objc func seg2(notification : Notification) {
        segEvents.setTitle("Past(\(notification.userInfo?["numberP"] ?? 0))", forSegmentAt: 1)
    }
    
    @objc func seg1(notification : Notification) {
        segEvents.setTitle("CurrentAndUpComing(\(notification.userInfo?["numberC"] ?? 0))", forSegmentAt: 0)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        for i in 0..<scrollTab.subviews.count {
            scrollTab.subviews[i].frame = CGRect(x: CGFloat(i) * view.frame.width, y: 0, width: view.frame.width, height: scrollTab.frame.height)
        }
    }
    
    private func setupScrollTab() {
        let eventsCurrentVC = EventsCurrentAndUpComing(nibName: "EventsCurrentAndUpComing", bundle: nil)
        addChild(eventsCurrentVC)
        scrollTab.addSubview(eventsCurrentVC.view)
        eventsCurrentVC.didMove(toParent: self)
        
        let eventsPassVC = SearchEventsPast(nibName: "SearchEventsPast", bundle: nil)
        addChild(eventsPassVC)
        scrollTab.addSubview(eventsPassVC.view)
        eventsPassVC.didMove(toParent: self)
    }
        
    @IBAction func backButton(_ sender: UIButton) {
        tabBarController?.tabBar.isHidden = false
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func deleteButton(_ sender: UIButton) {
        searchTF.text = ""
    }
    
    @IBAction func segAction(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            scrollTab.contentOffset.x = 0
        } else {
            scrollTab.contentOffset.x = view.frame.width
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}

extension SearchVC: UITextFieldDelegate {
 
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let text = textField.text else {
            textField.resignFirstResponder()
            return true
        }
        NotificationCenter.default.post(name: NSNotification.Name("key"), object: self, userInfo: ["text" : text])
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        NotificationCenter.default.post(name: NSNotification.Name("deleteData"), object: self)
        return true
    }
}


    
    

