//
//  EventsCategoryByPopular.swift
//  MockProject
//
//  Created by AnhDCT on 9/24/19.
//  Copyright © 2019 AnhDCT. All rights reserved.
//

import UIKit

class EventsCategoryByPopular: UITableViewController {
    private var id : Int
    private var pageIndex = 1
    private var pageSize = 10
    private var arrData = [EventsStruct]()
    var number : ((Int) -> Void)?
    private let activityIndicatorView = UIActivityIndicatorView(style: .gray)
    private let spinner = UIActivityIndicatorView(style: .gray)
    private var isLoading = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerForCell()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 300
        tableView.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimating()
        getApi()
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(refreshData), for: .valueChanged)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        activityIndicatorView.frame = view.frame
    }
    
    @objc func refreshData() {
        arrData = []
        pageIndex = 1
        getApi()
        tableView.reloadData()
        refreshControl?.endRefreshing()
    }

    private func getApi() {
        let url = "\(baseURL)listEventsByCategory?token=\(User.instance.token ?? "")&pageIndex=\(pageIndex)&category_id=\(id)&pageSize=\(pageSize)"
        getGenericData(urlString: url) { (json: PopularStruct) in
            DispatchQueue.main.async {
                json.response.events.forEach({ (event) in
                    self.arrData.append(event)
                    self.arrData.sort(by: { (a, b) -> Bool in
                        a.going_count! > b.going_count!
                    })
                    self.number?(self.arrData.count)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Po"), object: self, userInfo: ["T1": self.arrData.count])
                })
                self.tableView.reloadData()
                self.activityIndicatorView.stopAnimating()
                self.spinner.stopAnimating()
                self.tableView.tableFooterView?.isHidden = true
                self.isLoading = false
            }
        }
    }
    
    private func loadMore() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.pageIndex += 1
            self.getApi()
        }
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        if !isLoading && offsetY + 10 > contentHeight - scrollView.frame.size.height {
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            tableView.tableFooterView = spinner
            tableView.tableFooterView?.isHidden = false
            isLoading = true
            loadMore()
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PopularTableViewCell", for: indexPath) as! PopularTableViewCell
        cell.setupData(events: arrData[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let eventsDetailVC = EventsDetailViewController(id: arrData[indexPath.row].id ?? 1)
        present(eventsDetailVC, animated: true, completion: nil)
    }
    
    private func registerForCell(){
        self.tableView.register(UINib(nibName: "PopularTableViewCell", bundle: nil), forCellReuseIdentifier: "PopularTableViewCell")
    }
    
    init(id: Int) {
        self.id = id
        super.init(nibName: "EventsCategoryByPopular", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
