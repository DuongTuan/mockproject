//
//  SignUpVC.swift
//  MockProject
//
//  Created by AnhDCT on 9/19/19.
//  Copyright © 2019 AnhDCT. All rights reserved.
//

import UIKit

extension UILabel {
    
    func underline() {
        if let textString = self.text {
            let attributedString = NSMutableAttributedString(string: textString)
            attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: attributedString.length))
            attributedText = attributedString
        }
    }
}

class SignUpVC: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var signupView: UIButton!
    @IBOutlet weak var loginView: UIButton!
    @IBOutlet weak var alertEmail: UILabel!
    @IBOutlet weak var alertPassword: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loginView.titleLabel?.underline()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        signupView.layer.cornerRadius = 5
        signupView.isEnabled = false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    private func alert(message : String) {
        let a = UIAlertController(title: "Lỗi", message: message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        a.addAction(cancel)
        present(a, animated: true, completion: nil)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        checkTF()
        switch textField {
        case emailTF:
            guard let email = emailTF.text, email.count > 0 else {
                alertEmail.isHidden = true
                return
            }
            if isValidEmail(emailStr: email) == false {
                alertEmail.isHidden = false
            } else {
                alertEmail.isHidden = true
            }

        case passwordTF:
            guard let pass = passwordTF.text, pass.count > 0 else {
                alertPassword.isHidden = true
                return
            }
            if pass.count > 16 || pass.count < 6 {
                alertPassword.isHidden = false
            } else {
                alertPassword.isHidden = true
            }

        default:
            break
        }
    }
    
    private func isValidEmail(emailStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: emailStr)
    }
    
    private func checkTF() {
        guard let name = nameTF.text, let email = emailTF.text, let pass = passwordTF.text else {return}
        if name.count > 0, email.count > 0, pass.count > 0 {
            signupView.isEnabled = true
        }
    }
    
    @IBAction func signupButton(_ sender: UIButton) {
        let url = "\(baseURL)register"
        postGenericData(urlString: url, parameters: ["name" : nameTF.text, "email" : emailTF.text, "password": passwordTF.text]) { (json: ResponseSample) in
            DispatchQueue.main.async {
                if json.status == 1 {
                    User.instance.token = json.response?.token
                    User.instance.login = true
                    guard let window = UIApplication.shared.keyWindow else {
                        return
                    }
                    window.rootViewController = TabBarVC.instance
                    TabBarVC.instance.selectedIndex = 3
                } else {
                    self.alert(message: json.error_message ?? "Can not sign up, pleas try again")
                }
            }
        }
    }
    
    @IBAction func loginButton(_ sender: UIButton) {
        
        let login = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        navigationController?.pushViewController(login, animated: true)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
