//
//  InfoNewsViewController.swift
//  MockProject
//
//  Created by AnhDCT on 9/16/19.
//  Copyright © 2019 AnhDCT. All rights reserved.
//

import UIKit
import WebKit
class InfoNewsViewController: UIViewController {
    
    var link : String
    let activityIndicatorView = UIActivityIndicatorView(style: .gray)
    @IBOutlet weak var browse: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        browse.navigationDelegate = self
        view.addSubview(activityIndicatorView)
        activityIndicatorView.center = view.center
        activityIndicatorView.startAnimating()
        guard let url = URL(string: link) else {return}
        let req = URLRequest(url: url)
        browse.load(req)
        
    }
    
    init(link : String) {
        self.link = link
        super.init(nibName: "InfoNewsViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @IBAction func back(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
}

extension InfoNewsViewController : WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        activityIndicatorView.stopAnimating()
    }
}
