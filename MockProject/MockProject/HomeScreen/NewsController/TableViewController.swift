//
//  TableViewController.swift
//  MockProject
//
//  Created by AnhDCT on 9/11/19.
//  Copyright © 2019 AnhDCT. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {
    private var arrayNews = [NewsStruct]()
    private var pageIndex = 1
    private var pageSize = 10
    private let activityIndicatorView = UIActivityIndicatorView(style: .gray)
    private let spinner = UIActivityIndicatorView(style: .gray)
    private var isLoading = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 350
        getApi()
        registerForCell()
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        tableView.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimating()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        activityIndicatorView.center = view.center
    }
    
    private func getApi(){
        let api = "\(baseURL)listNews?pageIndex=\(pageIndex)&pageSize=\(pageSize)"
        getGenericData(urlString: api) { (json: JsonStruct ) in
            DispatchQueue.main.async {
                json.response.news.forEach({ (news) in
                    self.arrayNews.append(news)
                })
                self.arrayNews.sort(by: { (a, b) -> Bool in
                    a.publish_date > b.publish_date
                })
                self.tableView.reloadData()
                self.activityIndicatorView.stopAnimating()
                self.spinner.stopAnimating()
                self.tableView.tableFooterView?.isHidden = true
                self.isLoading = false
            }
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayNews.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsTableViewCell", for: indexPath) as! NewsTableViewCell
        cell.setData(news: arrayNews[indexPath.row])
        return cell
    }

    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if !isLoading && indexPath.row == arrayNews.count - 1 {
            spinner.startAnimating()
            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
            tableView.tableFooterView = spinner
            tableView.tableFooterView?.isHidden = false
            isLoading = true
            loadMore()
        }
    }
    
    private func loadMore() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.pageIndex += 1
            self.getApi()
        }
    }
    
    @objc func refreshData() {
        arrayNews = []
        pageIndex = 1
        getApi()
        tableView.reloadData()
        refreshControl?.endRefreshing()
    }
    
    private func registerForCell() {
        self.tableView.register(UINib(nibName: "NewsTableViewCell", bundle: nil), forCellReuseIdentifier: "NewsTableViewCell")
    }
}
extension TableViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let infoVC = InfoNewsViewController(link: arrayNews[indexPath.row].detail_url)
        present(infoVC, animated: true, completion: nil)
    }
}
