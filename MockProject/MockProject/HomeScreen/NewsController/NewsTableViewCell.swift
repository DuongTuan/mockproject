//
//  NewsTableViewCell.swift
//  MockProject
//
//  Created by AnhDCT on 9/11/19.
//  Copyright © 2019 AnhDCT. All rights reserved.
//

import UIKit

class NewsTableViewCell: UITableViewCell {
    @IBOutlet weak var imageNews: UIImageView!
    @IBOutlet weak var titleNews: UILabel!
    @IBOutlet weak var descriptionNews: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
       
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    fileprivate func setDescriptionNews(_ news: NewsStruct) {
        let start = news.publish_date
        let currentDate  = Date()
        let dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        guard let startDate = dateFormatter.date(from: start) else {return}
        let units = Set<Calendar.Component>([.year,.month,.day,.weekOfYear])
        let components = Calendar.current.dateComponents(units, from: startDate, to: currentDate)
        guard let year = components.year, let month = components.month, let week = components.weekOfYear, let day = components.day else { return }
        switch news.author {
        case "":
            if year > 0 {
                descriptionNews.text = "\(year)" + " years ago on " + news.feed
            } else if month > 0 {
                descriptionNews.text = "\(month)" + " months ago on " + news.feed
            } else if week > 0 {
                descriptionNews.text = "\(week)" + " weeks ago on " + news.feed
            } else if day > 0 {
                descriptionNews.text = "\(day)" + " days ago on " + news.feed
            }
        default:
            if year > 0 {
                descriptionNews.text = "\(year)" + " years ago by " + news.author + " on " + news.feed
            } else if month > 0 {
                descriptionNews.text = "\(month)" + " months ago by " + news.author + " on " + news.feed
            } else if week > 0 {
                descriptionNews.text = "\(week)" + " weeks ago by " + news.author + " on " + news.feed
            } else if day > 0 {
                descriptionNews.text = "\(day)" + " days ago by " + news.author + " on " + news.feed
            }
        }
    }
    
    func setData(news : NewsStruct) {
        
        setDescriptionNews(news)

        titleNews.text = news.title
        
        guard let url = URL(string: news.thumb_img) else {return}
        ImageService.getImage(withUrl: url) { (image) in
            self.imageNews.image = image
        }
    }
    
}
