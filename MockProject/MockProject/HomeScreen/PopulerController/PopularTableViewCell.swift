//
//  PopularTableViewCell.swift
//  MockProject
//
//  Created by AnhDCT on 9/11/19.
//  Copyright © 2019 AnhDCT. All rights reserved.
//

import UIKit

class PopularTableViewCell: UITableViewCell {
    @IBOutlet weak var popularImage: UIImageView!
    @IBOutlet weak var nameEvents: UILabel!
    @IBOutlet weak var descriptionEvents: UILabel!
    @IBOutlet weak var dateEvents: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupData(events : EventsStruct) {
        guard let start = events.schedule_start_date, let end = events.schedule_end_date else {return}
        let currentDate  = Date()
        let dateFormat = "yyyy-MM-dd"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        guard let startDate = dateFormatter.date(from: start) else {return}
        if startDate > currentDate {
            dateEvents.text = start + " - \(events.going_count ?? 0) người tham gia"
        } else {
            dateEvents.text = end + " - \(events.going_count ?? 0) người tham gia"
        }
        nameEvents.text = events.name
        descriptionEvents.text = events.description_html?.html2String
        guard let imageString = events.photo else {
            popularImage.image = #imageLiteral(resourceName: "default")
            return
        }
        guard let url = URL(string: imageString) else {return}
        ImageService.getImage(withUrl: url) { (image) in
            self.popularImage.image = image
        }
        
    }
}

extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

extension String {
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}
