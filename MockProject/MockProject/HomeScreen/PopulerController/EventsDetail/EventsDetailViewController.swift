//
//  EventsDetailViewController.swift
//  MockProject
//
//  Created by AnhDCT on 9/16/19.
//  Copyright © 2019 AnhDCT. All rights reserved.
//

import UIKit
extension UIView {
    func constraintToAllSides(of container: UIView) {
        translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            leadingAnchor.constraint(equalTo: container.leadingAnchor),
            trailingAnchor.constraint(equalTo: container.trailingAnchor),
            topAnchor.constraint(equalTo: container.topAnchor),
            bottomAnchor.constraint(equalTo: container.bottomAnchor)
            ])
    }
}


class EventsDetailViewController: UIViewController {
    @IBOutlet weak var navi: UINavigationBar!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageEvents: UIImageView!
    @IBOutlet weak var labelAddress: UILabel!
    @IBOutlet weak var lableNameEvent: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var labelAddress2: UILabel!
    @IBOutlet weak var labelCategories: UILabel!
    @IBOutlet weak var labelLocation: UILabel!
    @IBOutlet weak var labelContact: UILabel!
    @IBOutlet weak var viewWentButton: UIButton!
    @IBOutlet weak var viewGoingButton: UIButton!
    @IBOutlet weak var viewFollowButton: UIButton!
    @IBOutlet weak var viewNearBy: UIView!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint! {
        didSet {
            heightConstraint.constant = 200
        }
    }
    
    let view1 = UIView()
    let activityIndicatorView = UIActivityIndicatorView(style: .gray)
    var id : Int
    var events:EventsStr?
    var blurEffectView = UIVisualEffectView()

    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.delegate = self
        let heightImg = imageEvents.frame.height
        scrollView.contentInset = UIEdgeInsets(top: heightImg , left: 0, bottom: 0, right: 0)
        addSubview()
        getApi()
        setStatusLabelDescription()
    }
    
    func addSubview() {
        activityIndicatorView.startAnimating()
        view1.addSubview(activityIndicatorView)
        view.addSubview(view1)
        view1.backgroundColor = .white
    }
    
    func addSubViewForViewNearByEvents() {
        guard let lat = events?.venue.geo_lat, let long = events?.venue.geo_long else { return }
        let NearEventVC = NearByEventsVC(latitude: lat, longitude: long)
        addChild(NearEventVC)
        viewNearBy.addSubview(NearEventVC.view)
        NearEventVC.view.constraintToAllSides(of: viewNearBy)
        NearEventVC.didMove(toParent: self)
    }
    
    
    func getApi() {
        let api = "\(baseURL)getDetailEvent?token=\(User.instance.token ?? "")&event_id=\(id)"
        getGenericData(urlString: api) { (json: EventsModel) in
            self.events = json.response.events
            DispatchQueue.main.async {
                self.setupData(events: self.events!)
                self.setupViewButton(events: json)
                self.addSubViewForViewNearByEvents()
                self.view1.isHidden = true
            }
        }
    }
    
    func setupData(events: EventsStr) {
        
        labelAddress.text = events.venue.name
        lableNameEvent.text = events.name
        guard let start = events.schedule_start_date else {return}
        let currentDate  = Date()
        let dateFormat = "yyyy-MM-dd"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        guard let startDate = dateFormatter.date(from: start) else {return}
        if startDate < currentDate {
            labelTime.text = start + " - \(events.going_count ?? 0) người tham gia"
        } else {
            labelTime.text = events.schedule_end_date ?? "" + " - \(events.going_count ?? 0) người tham gia"
        }
        labelDescription.text = events.description_html?.html2String
        labelAddress2.text = events.venue.name
        labelCategories.text = events.category.name
        labelLocation.text = events.venue.contact_address
        labelContact.text = events.artist
        guard let imageString = events.photo else {
            imageEvents.image = #imageLiteral(resourceName: "default")
            return
        }
        guard let url = URL(string: imageString) else {return}
        ImageService.getImage(withUrl: url) { (image) in
            self.imageEvents.image = image
        }
    }
    fileprivate func addBlurView() {
        let blurEffect = UIBlurEffect(style: .prominent)
        blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.alpha = 0.8
        labelDescription.addSubview(blurEffectView)
        labelDescription.sendSubviewToBack(blurEffectView)
        blurEffectView.frame = CGRect(x: labelDescription.bounds.minX, y: labelDescription.bounds.maxY * 2, width: labelDescription.bounds.width, height: labelDescription.bounds.height/2)

        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    private func setStatusLabelDescription() {
        labelDescription.numberOfLines = 4
        labelDescription.isUserInteractionEnabled = true
        addBlurView()
        
        let pan = UITapGestureRecognizer(target: self, action: #selector(swapLine))
        labelDescription.addGestureRecognizer(pan)
    }
    
    private func alert(title : String, message : String){
        let a = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        a.addAction(cancel)
        present(a, animated: true, completion: nil)
    }
    
    private func setupViewButton(events : EventsModel){
        viewFollowButton.layer.cornerRadius = 5
        if User.instance.login == false {
            viewGoingButton.backgroundColor = .white
            viewWentButton.backgroundColor = .white
        } else {
            switch events.response.events.my_status {
            case 1:
                viewGoingButton.backgroundColor = .red
                viewGoingButton.isEnabled = false
                viewWentButton.backgroundColor = .white
            case 2:
                viewWentButton.backgroundColor = .yellow
                viewWentButton.isEnabled = false
                viewGoingButton.backgroundColor = .white
            default:
                viewGoingButton.backgroundColor = .white
                viewWentButton.backgroundColor = .white

            }
        }
    }
    
    @objc func swapLine() {
        switch labelDescription.numberOfLines {
        case 4:
            blurEffectView.alpha = 0
            labelDescription.numberOfLines = 0
            labelDescription.reloadInputViews()
        default:
            blurEffectView.alpha = 0.8
            labelDescription.numberOfLines = 4
            labelDescription.reloadInputViews()
        }
    }

    @IBAction func followButton(_ sender: UIButton) {
        if User.instance.login == false {
            let loginVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginVC")
            present(loginVC, animated: true, completion: nil)
        } else{
            let urlString = "\(baseURL)doFollowVenue"
            postGenericData(urlString: urlString, parameters: ["token": User.instance.token, "venue_id": events?.id]) { (json: ResponseSample) in
                DispatchQueue.main.async {
                    if json.status == 1 {
                        self.viewFollowButton.titleLabel?.text = "Followed"
                    } else {
                        self.alert(title: "Error", message: json.error_message ?? "a")
                    }
                }
            }
        }
    }
    
    @IBAction func closeButton(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
   
    @IBAction func goingButton(_ sender: UIButton) {
        if User.instance.login == false {
            let loginVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginVC")
            present(loginVC, animated: true, completion: nil)
        } else {
            let url = "\(baseURL)doUpdateEvent"
            postGenericData(urlString: url, parameters: ["token": User.instance.token, "status": 1, "event_id": events?.id]) { (json: ResponseSample) in
                DispatchQueue.main.async {
                    self.viewGoingButton.backgroundColor = .red
                    self.viewGoingButton.isEnabled = false
                    self.viewWentButton.backgroundColor = .white
                    self.viewWentButton.isEnabled = true
                }
            }
        }
    }
    
    @IBAction func wentButton(_ sender: UIButton) {
        if User.instance.login == false {
            let loginVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginVC")
            present(loginVC, animated: true, completion: nil)
        } else {
            let url = "\(baseURL)doUpdateEvent"
            postGenericData(urlString: url, parameters: ["token": User.instance.token, "status": 2, "event_id": events?.id]) { (json: ResponseSample) in
                DispatchQueue.main.async {
                    self.viewWentButton.backgroundColor = .yellow
                    self.viewWentButton.isEnabled = false
                    self.viewGoingButton.backgroundColor = .white
                    self.viewGoingButton.isEnabled = true
                }
            }
        }
    }
    
    init(id: Int) {
        self.id = id
        super.init(nibName: "EventsDetailViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
extension EventsDetailViewController: UIScrollViewDelegate {
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        view1.frame = view.frame
        activityIndicatorView.frame = view.frame
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let y = 200 - (scrollView.contentOffset.y + 200)
        let height = min(max(y, 0), 300)
        heightConstraint.constant = height
    }
}
