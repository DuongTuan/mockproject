//
//  Venue.swift
//  MockProject
//
//  Created by AnhDCT on 10/1/19.
//  Copyright © 2019 AnhDCT. All rights reserved.
//

import UIKit
import MapKit
import Contacts

class Venue: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    let title: String?
    let locationName: String?
    let status: Int?
    let id: Int
    init(title: String, locationName: String, coordinate : CLLocationCoordinate2D, status: Int, id: Int){
        self.coordinate = coordinate
        self.title = title
        self.locationName = locationName
        self.status = status
        self.id = id
        super.init()
    }
    var subtitle: String? {
        return locationName
    }
    
    class func from(json: Event) -> Venue {
        let id = json.id
        let title = json.name ?? ""
        let locationName = json.venue.contact_address
        let long = Double(json.venue.geo_long) ?? 0.0
        let lat = Double(json.venue.geo_lat) ?? 0.0
        let status = json.my_status ?? 0
        let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
        return Venue(title: title, locationName: locationName, coordinate: coordinate, status: status, id: id)
    }
    
    func mapItem() -> MKMapItem {
        let addressDictionary = [String(CNPostalAddressStreetKey) : subtitle]
        let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: addressDictionary as [String : Any])
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = "\(title ?? "") \(subtitle ?? "")"
        return mapItem
    }
}


