//
//  CallAPI.swift
//  MockProject
//
//  Created by AnhDCT on 10/22/19.
//  Copyright © 2019 AnhDCT. All rights reserved.
//

import Foundation

let baseURL = "http://4aff10d0.ngrok.io/18175d1_mobile_100_fresher/public/api/v0/"

func postGenericData<T: Decodable> (urlString: String, parameters:[String: Any?], completion: @escaping (T) -> ()) {
    guard let url = URL(string: urlString) else { return }
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: []) else { return }
    request.httpBody = httpBody
    URLSession.shared.dataTask(with: request) { (data, response, error) in
        if let data = data {
            do {
                let json = try JSONDecoder().decode(T.self, from: data)
                completion(json)
            } catch let err{
                print(err.localizedDescription)
            }
        }
    }.resume()
}

func getGenericData<T: Decodable>(urlString: String, completion: @escaping (T) -> ()) {
    guard let url = URL(string: urlString) else { return }
    URLSession.shared.dataTask(with: url) { (data, response, error) in
        guard let data = data else { return }
        do {
            let json = try JSONDecoder().decode(T.self, from: data)
            completion(json)
        } catch let err {
            print(err.localizedDescription)
        }
    }.resume()
}
